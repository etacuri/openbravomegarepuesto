//Sqlc generated V1.O00-1
package com.atrums.ventas.pedido.actionhandler;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class ProductosFacturacionData implements FieldProvider {
static Logger log4j = Logger.getLogger(ProductosFacturacionData.class);
  private String InitRecordNumber="0";
  public String dato1;
  public String dato2;
  public String dato3;
  public String dato4;
  public String dato5;
  public String dato6;
  public String dato7;
  public String dato8;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("dato1"))
      return dato1;
    else if (fieldName.equalsIgnoreCase("dato2"))
      return dato2;
    else if (fieldName.equalsIgnoreCase("dato3"))
      return dato3;
    else if (fieldName.equalsIgnoreCase("dato4"))
      return dato4;
    else if (fieldName.equalsIgnoreCase("dato5"))
      return dato5;
    else if (fieldName.equalsIgnoreCase("dato6"))
      return dato6;
    else if (fieldName.equalsIgnoreCase("dato7"))
      return dato7;
    else if (fieldName.equalsIgnoreCase("dato8"))
      return dato8;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static ProductosFacturacionData[] methodSeleccionardummy(ConnectionProvider connectionProvider, String fpvFacturaCotId)    throws ServletException {
    return methodSeleccionardummy(connectionProvider, fpvFacturaCotId, 0, 0);
  }

  public static ProductosFacturacionData[] methodSeleccionardummy(ConnectionProvider connectionProvider, String fpvFacturaCotId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select d.dummy as dato1, " +
      "			 d.dummy as dato2, " +
      "			 d.dummy as dato3, " +
      "			 d.dummy as dato4," +
      "			 d.dummy as dato5," +
      "			 d.dummy as dato6," +
      "			 d.dummy as dato7," +
      "			 d.dummy as dato8" +
      "	  from dual d " +
      "	  where d.dummy = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ProductosFacturacionData objectProductosFacturacionData = new ProductosFacturacionData();
        objectProductosFacturacionData.dato1 = UtilSql.getValue(result, "dato1");
        objectProductosFacturacionData.dato2 = UtilSql.getValue(result, "dato2");
        objectProductosFacturacionData.dato3 = UtilSql.getValue(result, "dato3");
        objectProductosFacturacionData.dato4 = UtilSql.getValue(result, "dato4");
        objectProductosFacturacionData.dato5 = UtilSql.getValue(result, "dato5");
        objectProductosFacturacionData.dato6 = UtilSql.getValue(result, "dato6");
        objectProductosFacturacionData.dato7 = UtilSql.getValue(result, "dato7");
        objectProductosFacturacionData.dato8 = UtilSql.getValue(result, "dato8");
        objectProductosFacturacionData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectProductosFacturacionData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ProductosFacturacionData objectProductosFacturacionData[] = new ProductosFacturacionData[vector.size()];
    vector.copyInto(objectProductosFacturacionData);
    return(objectProductosFacturacionData);
  }

  public static ProductosFacturacionData[] methodCrearLineaProducto(ConnectionProvider connectionProvider, String mProductId, String fpvFacturaCotId)    throws ServletException {
    return methodCrearLineaProducto(connectionProvider, mProductId, fpvFacturaCotId, 0, 0);
  }

  public static ProductosFacturacionData[] methodCrearLineaProducto(ConnectionProvider connectionProvider, String mProductId, String fpvFacturaCotId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  SELECT fpv_crear_producto(?,?) as dato1 " +
      "	  FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ProductosFacturacionData objectProductosFacturacionData = new ProductosFacturacionData();
        objectProductosFacturacionData.dato1 = UtilSql.getValue(result, "dato1");
        objectProductosFacturacionData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectProductosFacturacionData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ProductosFacturacionData objectProductosFacturacionData[] = new ProductosFacturacionData[vector.size()];
    vector.copyInto(objectProductosFacturacionData);
    return(objectProductosFacturacionData);
  }
}
