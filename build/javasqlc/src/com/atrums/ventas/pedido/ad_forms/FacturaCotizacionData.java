//Sqlc generated V1.O00-1
package com.atrums.ventas.pedido.ad_forms;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class FacturaCotizacionData implements FieldProvider {
static Logger log4j = Logger.getLogger(FacturaCotizacionData.class);
  private String InitRecordNumber="0";
  public String dato1;
  public String dato2;
  public String dato3;
  public String dato4;
  public String dato5;
  public String dato6;
  public String dato7;
  public String dato8;
  public String dato9;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("dato1"))
      return dato1;
    else if (fieldName.equalsIgnoreCase("dato2"))
      return dato2;
    else if (fieldName.equalsIgnoreCase("dato3"))
      return dato3;
    else if (fieldName.equalsIgnoreCase("dato4"))
      return dato4;
    else if (fieldName.equalsIgnoreCase("dato5"))
      return dato5;
    else if (fieldName.equalsIgnoreCase("dato6"))
      return dato6;
    else if (fieldName.equalsIgnoreCase("dato7"))
      return dato7;
    else if (fieldName.equalsIgnoreCase("dato8"))
      return dato8;
    else if (fieldName.equalsIgnoreCase("dato9"))
      return dato9;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static FacturaCotizacionData[] methodSeleccionardummy(ConnectionProvider connectionProvider, String param1)    throws ServletException {
    return methodSeleccionardummy(connectionProvider, param1, 0, 0);
  }

  public static FacturaCotizacionData[] methodSeleccionardummy(ConnectionProvider connectionProvider, String param1, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  SELECT d.dummy as dato1, " +
      "			 d.dummy as dato2, " +
      "			 d.dummy as dato3, " +
      "			 d.dummy as dato4," +
      "			 d.dummy as dato5," +
      "			 d.dummy as dato6," +
      "			 d.dummy as dato7," +
      "			 d.dummy as dato8," +
      "			 d.dummy as dato9" +
      "	  FROM dual d " +
      "	  WHERE d.dummy = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaCotizacionData objectFacturaCotizacionData = new FacturaCotizacionData();
        objectFacturaCotizacionData.dato1 = UtilSql.getValue(result, "dato1");
        objectFacturaCotizacionData.dato2 = UtilSql.getValue(result, "dato2");
        objectFacturaCotizacionData.dato3 = UtilSql.getValue(result, "dato3");
        objectFacturaCotizacionData.dato4 = UtilSql.getValue(result, "dato4");
        objectFacturaCotizacionData.dato5 = UtilSql.getValue(result, "dato5");
        objectFacturaCotizacionData.dato6 = UtilSql.getValue(result, "dato6");
        objectFacturaCotizacionData.dato7 = UtilSql.getValue(result, "dato7");
        objectFacturaCotizacionData.dato8 = UtilSql.getValue(result, "dato8");
        objectFacturaCotizacionData.dato9 = UtilSql.getValue(result, "dato9");
        objectFacturaCotizacionData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaCotizacionData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaCotizacionData objectFacturaCotizacionData[] = new FacturaCotizacionData[vector.size()];
    vector.copyInto(objectFacturaCotizacionData);
    return(objectFacturaCotizacionData);
  }

  public static FacturaCotizacionData[] methodCrearFactura(ConnectionProvider connectionProvider, String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9, String param10, String param11, String param12, String param13)    throws ServletException {
    return methodCrearFactura(connectionProvider, param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13, 0, 0);
  }

  public static FacturaCotizacionData[] methodCrearFactura(ConnectionProvider connectionProvider, String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9, String param10, String param11, String param12, String param13, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  SELECT fpv_crear_factura(?,?,?,?,?,?,?,?,?,?,?,?,?::numeric) as dato1 " +
      "	  FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param3);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param4);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param5);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param6);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param7);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param8);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param9);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param10);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param11);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param12);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param13);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaCotizacionData objectFacturaCotizacionData = new FacturaCotizacionData();
        objectFacturaCotizacionData.dato1 = UtilSql.getValue(result, "dato1");
        objectFacturaCotizacionData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaCotizacionData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaCotizacionData objectFacturaCotizacionData[] = new FacturaCotizacionData[vector.size()];
    vector.copyInto(objectFacturaCotizacionData);
    return(objectFacturaCotizacionData);
  }

  public static FacturaCotizacionData[] methodMetodoPagoAlto(ConnectionProvider connectionProvider, String param1)    throws ServletException {
    return methodMetodoPagoAlto(connectionProvider, param1, 0, 0);
  }

  public static FacturaCotizacionData[] methodMetodoPagoAlto(ConnectionProvider connectionProvider, String param1, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  SELECT fmp.fpv_metodopago_v_id as dato1, " +
      "	           fmp.name as dato2 " +
      "	  FROM fpv_metodopago_v fmp " +
      "	  WHERE fmp.ad_client_id = ?;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaCotizacionData objectFacturaCotizacionData = new FacturaCotizacionData();
        objectFacturaCotizacionData.dato1 = UtilSql.getValue(result, "dato1");
        objectFacturaCotizacionData.dato2 = UtilSql.getValue(result, "dato2");
        objectFacturaCotizacionData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaCotizacionData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaCotizacionData objectFacturaCotizacionData[] = new FacturaCotizacionData[vector.size()];
    vector.copyInto(objectFacturaCotizacionData);
    return(objectFacturaCotizacionData);
  }

  public static FacturaCotizacionData[] methodTotalMetodo(ConnectionProvider connectionProvider, String param1, String param2)    throws ServletException {
    return methodTotalMetodo(connectionProvider, param1, param2, 0, 0);
  }

  public static FacturaCotizacionData[] methodTotalMetodo(ConnectionProvider connectionProvider, String param1, String param2, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  SELECT fpv_total_cotizacion(?,?) as dato1 " +
      "	  FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param2);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaCotizacionData objectFacturaCotizacionData = new FacturaCotizacionData();
        objectFacturaCotizacionData.dato1 = UtilSql.getValue(result, "dato1");
        objectFacturaCotizacionData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaCotizacionData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaCotizacionData objectFacturaCotizacionData[] = new FacturaCotizacionData[vector.size()];
    vector.copyInto(objectFacturaCotizacionData);
    return(objectFacturaCotizacionData);
  }

  public static FacturaCotizacionData[] methodBuscarTercero(ConnectionProvider connectionProvider, String param1, String param2)    throws ServletException {
    return methodBuscarTercero(connectionProvider, param1, param2, 0, 0);
  }

  public static FacturaCotizacionData[] methodBuscarTercero(ConnectionProvider connectionProvider, String param1, String param2, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT " +
      "      ftp.fpv_tipopersona_id as dato1,  " +
      "      fti.fpv_tipoidentificacion_id as dato2, " +
      "      cb.em_co_nombres as dato3, " +
      "      cb.em_co_apellidos as dato4, " +
      "      cb.name as dato5, " +
      "      cb.name2 as dato6, " +
      "      au.email as dato7, " +
      "      au.phone as dato8, " +
      "      cl.address1 as dato9 " +
      "      FROM c_bpartner cb " +
      "      LEFT JOIN fpv_tipopersona ftp ON (cb.em_co_natural_juridico = ftp.value AND ftp.isactive = 'Y') " +
      "      LEFT JOIN fpv_tipoidentificacion fti ON (cb.em_co_tipo_identificacion = fti.value AND fti.isactive = 'Y') " +
      "      LEFT JOIN ad_user au ON (au.c_bpartner_id = cb.c_bpartner_id AND au.isactive = 'Y')" +
      "      LEFT JOIN c_bpartner_location cbl ON (cbl.c_bpartner_id = cb.c_bpartner_id AND cbl.isactive = 'Y') " +
      "      LEFT JOIN c_location cl ON (cl.c_location_id = cbl.c_location_id)" +
      "      WHERE cb.taxid = ?  " +
      "      AND cb.ad_client_id = ?;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param2);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaCotizacionData objectFacturaCotizacionData = new FacturaCotizacionData();
        objectFacturaCotizacionData.dato1 = UtilSql.getValue(result, "dato1");
        objectFacturaCotizacionData.dato2 = UtilSql.getValue(result, "dato2");
        objectFacturaCotizacionData.dato3 = UtilSql.getValue(result, "dato3");
        objectFacturaCotizacionData.dato4 = UtilSql.getValue(result, "dato4");
        objectFacturaCotizacionData.dato5 = UtilSql.getValue(result, "dato5");
        objectFacturaCotizacionData.dato6 = UtilSql.getValue(result, "dato6");
        objectFacturaCotizacionData.dato7 = UtilSql.getValue(result, "dato7");
        objectFacturaCotizacionData.dato8 = UtilSql.getValue(result, "dato8");
        objectFacturaCotizacionData.dato9 = UtilSql.getValue(result, "dato9");
        objectFacturaCotizacionData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaCotizacionData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaCotizacionData objectFacturaCotizacionData[] = new FacturaCotizacionData[vector.size()];
    vector.copyInto(objectFacturaCotizacionData);
    return(objectFacturaCotizacionData);
  }
}
