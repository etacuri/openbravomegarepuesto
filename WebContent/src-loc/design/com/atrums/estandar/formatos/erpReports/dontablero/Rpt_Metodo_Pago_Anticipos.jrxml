<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Rpt_Metodo_Pago_Anticipos" pageWidth="410" pageHeight="842" columnWidth="410" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0">
	<property name="ireport.zoom" value="3.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="ad_org_id" class="java.lang.String"/>
	<parameter name="fechaIni" class="java.util.Date"/>
	<parameter name="fechaFin" class="java.util.Date"/>
	<queryString>
		<![CDATA[(select (select sc.name from ad_org sc where sc.ad_org_id = $P{ad_org_id}) as sucursal,
        pm.name as metodo_pago,
        pm.fin_paymentmethod_id,
        coalesce(sum(p.generated_credit),0) as credito_generado,
        coalesce(sum(p.used_credit),0) as credito_usado,
        coalesce(sum((p.generated_credit - p.used_credit)),0) as credito_disponible
   from fin_paymentmethod pm
   left join fin_payment p on (p.fin_paymentmethod_id = pm.fin_paymentmethod_id
                                and p.isreceipt = 'Y'
                                and p.generated_credit > 0
                                and date(p.created) between date($P{fechaIni}) and date($P{fechaFin})
                                and p.ad_org_id = $P{ad_org_id})
   left join ad_org a on (p.ad_org_id = a.ad_org_id and p.ad_org_id = $P{ad_org_id})
  where pm.name not in ('Retención', 'Mixto', 'Con Retención')
  group by 1,2,3
  order by 1,2
 )
union all
(select (select sc.name from ad_org sc where sc.ad_org_id = $P{ad_org_id}) as sucursal,
        'Retenciòn' as metodo_pago,
        '0' as fin_paymentmethod_id,
        0 as credito_generado,
        0 as credito_usado,
        0 as credito_disponible
   from ad_org b
  where b.ad_org_id = $P{ad_org_id}
  )]]>
	</queryString>
	<field name="sucursal" class="java.lang.String"/>
	<field name="metodo_pago" class="java.lang.String"/>
	<field name="fin_paymentmethod_id" class="java.lang.String"/>
	<field name="credito_generado" class="java.math.BigDecimal"/>
	<field name="credito_usado" class="java.math.BigDecimal"/>
	<field name="credito_disponible" class="java.math.BigDecimal"/>
	<variable name="credito_generado" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{credito_generado}]]></variableExpression>
	</variable>
	<variable name="credito_disponible" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{credito_disponible}]]></variableExpression>
	</variable>
	<variable name="porcentaje" class="java.math.BigDecimal">
		<variableExpression><![CDATA[]]></variableExpression>
	</variable>
	<columnHeader>
		<band height="30" splitType="Stretch">
			<staticText>
				<reportElement mode="Opaque" x="0" y="0" width="200" height="30" backcolor="#CCCCCC"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Método de Pago]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="200" y="0" width="70" height="30" backcolor="#CCCCCC"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Crédito Generado]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="270" y="0" width="70" height="30" backcolor="#CCCCCC"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Crédito Disponible]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="340" y="0" width="70" height="30" backcolor="#CCCCCC"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Porcentaje Generado]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="15" splitType="Stretch">
			<textField>
				<reportElement x="0" y="0" width="200" height="15"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[new String("  ") + $F{metodo_pago}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="200" y="0" width="55" height="15"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{credito_generado}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="270" y="0" width="55" height="15"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{credito_disponible}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="255" y="0" width="15" height="15"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[$]]></text>
			</staticText>
			<staticText>
				<reportElement x="325" y="0" width="15" height="15"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[$]]></text>
			</staticText>
			<staticText>
				<reportElement x="395" y="0" width="15" height="15"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[%]]></text>
			</staticText>
			<textField evaluationTime="Auto" pattern="#,##0.00">
				<reportElement x="340" y="0" width="55" height="15">
					<printWhenExpression><![CDATA[!$F{credito_generado}.equals(new BigDecimal("0"))]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[($F{credito_generado}.multiply(new BigDecimal("100"))).divide($V{credito_generado},4)]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Auto" pattern="#,##0.00">
				<reportElement x="340" y="0" width="55" height="15">
					<printWhenExpression><![CDATA[$F{credito_generado}.equals(BigDecimal.ZERO)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[new BigDecimal("0")]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<lastPageFooter>
		<band/>
	</lastPageFooter>
	<summary>
		<band height="15">
			<rectangle>
				<reportElement x="340" y="0" width="70" height="15"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</rectangle>
			<rectangle>
				<reportElement x="270" y="0" width="70" height="15"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</rectangle>
			<rectangle>
				<reportElement x="200" y="0" width="70" height="15"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</rectangle>
			<textField pattern="#,##0.00">
				<reportElement x="200" y="0" width="55" height="15"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{credito_generado}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="270" y="0" width="55" height="15"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{credito_disponible}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="0" width="200" height="15"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Total Anticipos  ]]></text>
			</staticText>
			<staticText>
				<reportElement x="255" y="0" width="15" height="15"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[$]]></text>
			</staticText>
			<staticText>
				<reportElement x="325" y="0" width="15" height="15"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[$]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="340" y="0" width="55" height="15">
					<printWhenExpression><![CDATA[!$V{credito_generado}.equals(new BigDecimal("0"))]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[new BigDecimal("100")]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="395" y="0" width="15" height="15"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[%]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="340" y="0" width="55" height="15">
					<printWhenExpression><![CDATA[$V{credito_generado}.equals(new BigDecimal("0"))]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[new BigDecimal("0")]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
