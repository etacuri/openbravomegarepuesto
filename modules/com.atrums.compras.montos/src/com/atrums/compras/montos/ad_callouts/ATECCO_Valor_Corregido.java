package com.atrums.compras.montos.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.ParseException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class ATECCO_Valor_Corregido extends HttpSecureAppServlet {

  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {

      String strValorCorregido = vars.getStringParameter("inpvalorCorregido");
      String strCierreSierre = vars.getStringParameter("inpcierreSistema");

      try {
        printPage(response, vars, strValorCorregido, strCierreSierre);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      } catch (ParseException ex) {
        // TODO Auto-generated catch block
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strValorCorregido, String strCierreSierre)
      throws IOException, ServletException, ParseException {
    log4j.debug("Output: dataSheet");

    boolean validado = false;

    BigDecimal bdValorCorregido = new BigDecimal(strValorCorregido);
    BigDecimal bdCierreSistema = new BigDecimal(strCierreSierre);

    if (bdValorCorregido.doubleValue() == bdCierreSistema.doubleValue()) {
      validado = true;
    }

    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/compras/montos/ad_callouts/CallOut").createXmlDocument();

    StringBuffer result = new StringBuffer();
    result.append("var calloutName='ATECCO_Valor_Corregido';\n\n");

    result.append("var respuesta = new Array(");

    if (validado) {
      result.append("new Array(\"inpestadoCorregido\", \"" + "V" + "\")");
      result.append(");");
    } else {
      result.append("new Array(\"inpestadoCorregido\", \"" + "SV" + "\")");
      result.append(");");
    }

    // inject the generated code xmlDocument.setParameter("array",
    // result.toString());
    xmlDocument.setParameter("array", result.toString());
    xmlDocument.setParameter("frameName", "appFrame");

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }
}
