package com.atrums.compras.montos.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class ATECCO_Fecha_Valido extends HttpSecureAppServlet {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strDateOrdered = vars.getStringParameter("inpdateordered");

      try {
        printPage(response, vars, strDateOrdered);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      } catch (ParseException ex) {
        // TODO Auto-generated catch block
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strDateOrdered) throws IOException, ServletException, ParseException {
    log4j.debug("Output: dataSheet");

    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    Calendar c = Calendar.getInstance();
    c.setTime(sdf.parse(strDateOrdered));
    c.add(Calendar.DATE, 3);
    String auxStrDateOrdered = sdf.format(c.getTime());

    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/compras/montos/ad_callouts/CallOut").createXmlDocument();

    StringBuffer result = new StringBuffer();
    result.append("var calloutName='ATECCO_Fecha_Valido';\n\n");

    result.append("var respuesta = new Array(");

    if (auxStrDateOrdered != null) {
      result.append("new Array(\"inpvaliduntil\", \"" + auxStrDateOrdered + "\")");
      result.append(");");
    } else {
      result.append("new Array(\"inpvaliduntil\", \"" + null + "\")");
      result.append(");");
    }

    // inject the generated code xmlDocument.setParameter("array",
    // result.toString());
    xmlDocument.setParameter("array", result.toString());
    xmlDocument.setParameter("frameName", "appFrame");

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();

  }

}