package com.atrums.compras.montos.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class ATECCO_Metodo_Pago extends HttpSecureAppServlet {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strFinPaymentmethodId = vars.getStringParameter("inpfinPaymentmethodId");

      try {
        printPage(response, vars, strFinPaymentmethodId);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      } catch (ParseException ex) {
        // TODO Auto-generated catch block
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strFinPaymentmethodId) throws IOException, ServletException, ParseException {
    log4j.debug("Output: dataSheet");

    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/compras/montos/ad_callouts/CallOut").createXmlDocument();

    StringBuffer result = new StringBuffer();
    result.append("var calloutName='ATECCO_Metodo_Pago';\n\n");

    result.append("var respuesta = new Array(");

    if (strFinPaymentmethodId != null) {
      if (strFinPaymentmethodId.equals("EC105")) {
        result.append(
            "new Array(\"inpmPricelistId\", \"" + "3A12BCD9C9174E24B47B27ECE9A7C85E" + "\")");
        result.append(");");
      } else {
        result.append(
            "new Array(\"inpmPricelistId\", \"" + "AFBCE0080B1645E58094C860F78F2266" + "\")");
        result.append(");");
      }

    } else {
      result.append("new Array(\"inpmPricelistId\", \"" + null + "\")");
      result.append(");");
    }

    // inject the generated code xmlDocument.setParameter("array",
    // result.toString());
    xmlDocument.setParameter("array", result.toString());
    xmlDocument.setParameter("frameName", "appFrame");

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }
}
