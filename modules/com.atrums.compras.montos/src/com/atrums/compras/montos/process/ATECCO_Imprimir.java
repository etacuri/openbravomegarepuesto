package com.atrums.compras.montos.process;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.jfree.util.Log;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.xmlEngine.XmlDocument;

import com.atrums.felectronica.process.ATECFE_Funciones_Aux;

public class ATECCO_Imprimir extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String strCOrderId = vars.getStringParameter("inpcOrderId");

    if (vars.commandIn("DEFAULT")) {
      printPageDataSheet(response, vars, strCOrderId);
    } else if (vars.commandIn("PRINT_PDF")) {
      try {
        printPageHtml(response, vars, strCOrderId, "pdf");
      } catch (SQLException e) {
        // TODO Auto-generated catch block
        Log.warn(e.getMessage());
      }
    } else
      pageError(response);
  }

  private void printPageHtml(HttpServletResponse response, VariablesSecureApp vars,
      String strCOrderId, String strOutput) throws IOException, ServletException, SQLException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    response.setContentType("text/html; charset=UTF-8");

    ATECFE_Funciones_Aux opeaux = new ATECFE_Funciones_Aux();

    String sqlQuery = "SELECT c_doctypetarget_id, cpdt.templatelocation || '/' || cpdt.templatefilename AS templatelocation, cpdt.name "
        + "FROM c_order co "
        + "INNER JOIN c_doctype cd ON (co.c_doctypetarget_id = cd.c_doctype_id) "
        + "INNER JOIN c_poc_doctype_template cpdt ON (cd.c_doctype_id = cpdt.c_doctype_id) "
        + "WHERE co.c_order_id = '" + strCOrderId + "';";

    Connection connection = OBDal.getInstance().getConnection();
    PreparedStatement ps = connection.prepareStatement(sqlQuery);
    ResultSet rs = ps.executeQuery();

    String auxdoc = null;
    String auxname = null;

    while (rs.next()) {
      if (rs.getString("templatelocation") != null) {
        auxdoc = rs.getString("templatelocation");
        auxname = rs.getString("name");
      }
    }

    rs.close();
    ps.close();

    File flPdf = null;

    if (auxdoc != null) {
      flPdf = opeaux.generarPDF(this, auxdoc, "Factura", strCOrderId);
    }

    if (flPdf != null) {
      FileInputStream insStream = new FileInputStream(flPdf);

      String fileName = "Documento_" + auxname + ".pdf";

      response.setContentType("application/pdf");
      response.setHeader("Content-Disposition", "inline; filename=Documento_" + auxname + ".pdf");
      IOUtils.copy(insStream, response.getOutputStream());
      flPdf.exists();
      insStream.close();
      response.flushBuffer();

      if (log4j.isDebugEnabled())
        log4j.debug("Output: PopUp Download");
      String href = strDireccion + "/utility/DownloadReport.html?report=" + fileName;
      XmlDocument xmlDocument = xmlEngine
          .readXmlTemplate("org/openbravo/base/secureApp/PopUp_DownloadAndRefresh")
          .createXmlDocument();
      xmlDocument.setParameter("href", href);
      response.getOutputStream().println(xmlDocument.print());
      response.getOutputStream().close();
    }
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
      String strCOrderId) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();

    XmlDocument xmlDocument = null;

    xmlDocument = xmlEngine.readXmlTemplate("com/atrums/compras/montos/process/ATECCO_Imprimir")
        .createXmlDocument();

    ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "ATECCO_Imprimir", false, "", "", "",
        false, "ad_reports", strReplaceWith, false, true);
    toolbar.prepareSimpleToolBarTemplate();
    xmlDocument.setParameter("toolbar", toolbar.toString());
    try {
      WindowTabs tabs = new WindowTabs(this, vars,
          "com.atrums.compras.montos.process.ATECCO_Imprimir");
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      xmlDocument.setParameter("theme", vars.getTheme());
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "ATECCO_Imprimir.html",
          classInfo.id, classInfo.type, strReplaceWith, tabs.breadcrumb());
      xmlDocument.setParameter("navigationBar", nav.toString());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage("ATECCO_Imprimir");
      vars.removeMessage("ATECCO_Imprimir");
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }

    xmlDocument.setParameter("cOrderId", strCOrderId);

    out.println(xmlDocument.print());
    out.close();
  }

  public String getServletInfo() {
    return "Servlet ReportCashJR.";
  }
}
