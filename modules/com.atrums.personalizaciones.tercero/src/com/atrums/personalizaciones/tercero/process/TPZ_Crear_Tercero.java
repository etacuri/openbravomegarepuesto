package com.atrums.personalizaciones.tercero.process;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;

public class TPZ_Crear_Tercero extends HttpSecureAppServlet {

  /**
   *  
   */
  private static final long serialVersionUID = 1L;

  @Override
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strProcessId = vars.getStringParameter("inpProcessId");
      String strWindow = vars.getStringParameter("inpwindowId");
      String strTab = vars.getStringParameter("inpTabId");

      String strcInvoice = null;
      String strcOrder = null;

      try {
        strcInvoice = vars.getGlobalVariable("inpcInvoiceId", strWindow + "|C_Invoice_ID");
      } catch (Exception e) {
        // TODO: handle exception
      }

      if (strcInvoice == null) {
        try {
          strcOrder = vars.getGlobalVariable("inpcOrderId", strWindow + "|C_Order_ID");
        } catch (Exception e) {
          // TODO: handle exception
        }
      }

      printPage(response, vars, null, strcInvoice, strcOrder, strWindow, strTab, strProcessId);
    } else if (vars.commandIn("SAVE")) {
      String strInvoice = vars.getStringParameter("inpcInvoiceId");
      String strOrder = vars.getStringParameter("inpcOrderId");

      String strKey = null; // vars.getGlobalVariable("inpcInvoiceId", strWindow + "|C_Invoice_ID");
      String strTab = vars.getStringParameter("inpTabId");

      String strTipoPersona = vars.getStringParameter("inpTipoPersona");
      String strTipoIdentificacion = vars.getStringParameter("inpTipoIdentificacion");
      String strIdentificacion = vars.getStringParameter("inpIdentificacion");

      String strNombre = vars.getStringParameter("inpNombre");
      String strApellido = vars.getStringParameter("inpApellido");
      String strNombreComercial = vars.getStringParameter("inpNombreComercial");

      String strRazonSocial = vars.getStringParameter("inpRazonSocial");
      String strEmail = vars.getStringParameter("inpEmail");
      String strTelefono = vars.getStringParameter("inpTelefono");
      String strDireccionClie = vars.getStringParameter("inpDireccion");

      String strWindowPath = Utility.getTabURL(strTab, "R", true);
      if (strWindowPath.equals("")) {
        strWindowPath = strDefaultServlet;
      }

      OBError myError = processButton(vars, strKey, strInvoice, strOrder, strTipoPersona,
          strTipoIdentificacion, strIdentificacion, strNombre, strApellido, strNombreComercial,
          strRazonSocial, strEmail, strTelefono, strDireccionClie);
      log4j.debug(myError.getMessage());
      vars.setMessage(strTab, myError);
      printPageClosePopUp(response, vars, strWindowPath);
    }
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strKey,
      String strcInvoice, String strcOrder, String windowId, String strTab, String strProcessId)
      throws IOException, ServletException {

    try {
      ComboTableData comboTableDataTipoIdentificacion = new ComboTableData(vars, this, "TABLEDIR",
          "TPZ_TIPOIDENTIFICACION_ID", "", null,
          Utility.getContext(this, vars, "#User_Org", "MaterialReceiptPending"),
          Utility.getContext(this, vars, "#User_Client", "MaterialReceiptPending"), 0);

      ComboTableData comboTableDataTipoPersona = new ComboTableData(vars, this, "TABLEDIR",
          "TPZ_TIPOPERSONA_ID", "", null,
          Utility.getContext(this, vars, "#User_Org", "MaterialReceiptPending"),
          Utility.getContext(this, vars, "#User_Client", "MaterialReceiptPending"), 0);

      XmlDocument xmlDocument = xmlEngine
          .readXmlTemplate("com/atrums/personalizaciones/tercero/process/TPZ_Crear_Tercero")
          .createXmlDocument();
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("theme", vars.getTheme());
      xmlDocument.setParameter("key", strKey);
      xmlDocument.setParameter("cinvoiceid", strcInvoice);
      xmlDocument.setParameter("corderid", strcOrder);
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("tab", strTab);

      // xmlDocument.setParameter("tipopersona", "");
      xmlDocument.setData("reportTPZTipoPersona_ID", "liststructure",
          comboTableDataTipoPersona.select(false));
      xmlDocument.setData("reportTPZTipoIdentificacion_ID", "liststructure",
          comboTableDataTipoIdentificacion.select(false));

      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      out.println(xmlDocument.print());
      out.close();

    } catch (Exception ex) {
      // TODO: handle exception
      throw new ServletException(ex);
    }
  }

  private OBError processButton(VariablesSecureApp vars, String strKey, String strInvoice,
      String strOrder, String strTipoPersona, String strTipoIdentificacion,
      String strIdentificacion, String strNombre, String strApellido, String strNombreComercial,
      String strRazonSocial, String strEmail, String strTelefono, String strDireccionClien) {
    OBError myError = null;
    Connection conn = null;

    OBContext.setAdminMode(true);
    try {
      conn = OBDal.getInstance().getConnection();
      String sqlQuery = "";

      sqlQuery = "SELECT * FROM tpz_crear_tercero("
          + (strInvoice == null || strInvoice.equals("null") ? "null" : "'" + strInvoice + "'")
          + "," + (strOrder == null || strOrder.equals("null") ? "null" : "'" + strOrder + "'")
          + ",'" + strTipoPersona + "'" + ",'" + strTipoIdentificacion + "','" + strIdentificacion
          + "','" + strNombre + "','" + strApellido + "','" + strNombreComercial + "','"
          + strRazonSocial + "','" + strEmail + "','" + strTelefono + "','" + strDireccionClien
          + "')";

      PreparedStatement ps = conn.prepareStatement(sqlQuery);
      ps.execute();

      ps.close();
      OBDal.getInstance().commitAndClose();

      myError = new OBError();
      myError.setType("Success");
      myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();

      e.printStackTrace();
      log4j.warn("Rollback in transaction");
      myError = new OBError();
      myError.setType("Error");
      myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
      myError.setMessage("El proceso no se ejecuto correctamente: " + e.getMessage());
    } finally {
      OBContext.restorePreviousMode();
      if (conn != null) {
        try {
          conn.close();
        } catch (Exception e) {
        }
      }
    }

    return myError;
  }
}
