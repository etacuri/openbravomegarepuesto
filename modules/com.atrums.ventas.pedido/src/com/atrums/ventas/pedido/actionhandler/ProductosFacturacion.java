package com.atrums.ventas.pedido.actionhandler;

import java.util.Map;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.service.db.DbUtility;

public class ProductosFacturacion extends BaseProcessActionHandler {
  private static final Logger log = Logger.getLogger(ProductosFacturacion.class);

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {
    // TODO Auto-generated method stub
    VariablesSecureApp vars = RequestContext.get().getVariablesSecureApp();
    JSONObject result = null;

    OBContext.setAdminMode(true);

    try {
      result = new JSONObject();
      JSONObject request = new JSONObject(content);
      JSONObject params = request.getJSONObject("_params");

      JSONArray tableIds = params.getJSONArray("M_Product_ID");
      String strFpvFacturaCotId = request.getString("inpfpvFacturaCotId");

      if (tableIds != null) {
        crearProductos(result, tableIds, strFpvFacturaCotId, vars);
      }
    } catch (Exception e) {

      log.error("Error in Reset Accounting Action Handler", e);
      try {
        result = new JSONObject();
        Throwable ex = DbUtility.getUnderlyingSQLException(e);
        String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();
        JSONObject errorMessage = new JSONObject();
        errorMessage.put("severity", "error");
        errorMessage.put("text", message);
        result.put("message", errorMessage);
      } catch (Exception e2) {
        log.error(e.getMessage(), e2);
      }
    } finally {
      OBContext.restorePreviousMode();
    }

    return result;
  }

  private void crearProductos(JSONObject result, JSONArray tableIds, String strFpvFacturaCotId,
      VariablesSecureApp vars) throws JSONException, ServletException {

    for (int i = 0; i < tableIds.length(); i++) {
      String strMProductId = tableIds.getString(i);

      ProductosFacturacionData[] auxData = ProductosFacturacionData
          .methodCrearLineaProducto(new DalConnectionProvider(), strMProductId, strFpvFacturaCotId);

      if (auxData.length > 0) {
        log.info(auxData[0].dato1);
      }
    }

    JSONObject successMessage = new JSONObject();
    successMessage.put("severity", "success");
    StringBuilder message = new StringBuilder();
    message.append(OBMessageUtils.parseTranslation(new DalConnectionProvider(), vars,
        vars.getLanguage(), "Productos Procesados"));
    successMessage.put("text", message);
    result.put("message", successMessage);
  }

}
