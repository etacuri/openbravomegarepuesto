package com.atrums.ventas.pedido.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class TotalLinea extends HttpSecureAppServlet {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strPrecio = vars.getStringParameter("inpprecio");
      String strCantidad = vars.getStringParameter("inpcantidad");

      try {
        printPage(response, vars, strPrecio, strCantidad);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strPrecio,
      String strCantidad) throws IOException, ServletException {
    log4j.debug("Output: dataSheet");

    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/ventas/pedido/ad_callouts/CallOut").createXmlDocument();

    StringBuffer result = new StringBuffer();
    result.append("var calloutName='TotalLinea';\n\n");

    result.append("var respuesta = new Array(");

    BigDecimal precioBig = (strPrecio.equals("") ? BigDecimal.ZERO : new BigDecimal(strPrecio));

    BigDecimal cantidadBig = (strCantidad.equals("") ? BigDecimal.ZERO
        : new BigDecimal(strCantidad));

    BigDecimal subtotalBig = precioBig.multiply(cantidadBig);

    result.append("new Array(\"inpsubtotal\", \"" + subtotalBig + "\")");
    result.append(");");

    xmlDocument.setParameter("array", result.toString());
    xmlDocument.setParameter("frameName", "appFrame");

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }
}
