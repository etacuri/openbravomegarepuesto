/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.compras.montos.data;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
/**
 * Entity class for entity atecco_retiro (stored in table atecco_retiro).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class ateccoRetiro extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "atecco_retiro";
    public static final String ENTITY_NAME = "atecco_retiro";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_ATECCOCIERRECAJA = "ateccoCierrecaja";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_HORA = "hora";
    public static final String PROPERTY_NRODEPOSITO = "nroDeposito";
    public static final String PROPERTY_TOTALDEPOSITO = "totalDeposito";
    public static final String PROPERTY_PAYMENTMETHOD = "paymentMethod";
    public static final String PROPERTY_SALES = "sales";
    public static final String PROPERTY_NROCHEQUE = "nroCheque";
    public static final String PROPERTY_ENTREGADO = "entregado";
    public static final String PROPERTY_DEPOSITADO = "depositado";
    public static final String PROPERTY_FECHADEPOSITO = "fechaDeposito";
    public static final String PROPERTY_FINFINANCIALACCOUNT = "fINFinancialAccount";
    public static final String PROPERTY_FINPAYMENTFROM = "fINPaymentfrom";
    public static final String PROPERTY_FINPAYMENTTO = "fINPaymentto";

    public ateccoRetiro() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_TOTALDEPOSITO, new BigDecimal(0));
        setDefaultValue(PROPERTY_ENTREGADO, false);
        setDefaultValue(PROPERTY_DEPOSITADO, false);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public ateccoCierrecaja getAteccoCierrecaja() {
        return (ateccoCierrecaja) get(PROPERTY_ATECCOCIERRECAJA);
    }

    public void setAteccoCierrecaja(ateccoCierrecaja ateccoCierrecaja) {
        set(PROPERTY_ATECCOCIERRECAJA, ateccoCierrecaja);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Date getHora() {
        return (Date) get(PROPERTY_HORA);
    }

    public void setHora(Date hora) {
        set(PROPERTY_HORA, hora);
    }

    public String getNroDeposito() {
        return (String) get(PROPERTY_NRODEPOSITO);
    }

    public void setNroDeposito(String nroDeposito) {
        set(PROPERTY_NRODEPOSITO, nroDeposito);
    }

    public BigDecimal getTotalDeposito() {
        return (BigDecimal) get(PROPERTY_TOTALDEPOSITO);
    }

    public void setTotalDeposito(BigDecimal totalDeposito) {
        set(PROPERTY_TOTALDEPOSITO, totalDeposito);
    }

    public FIN_PaymentMethod getPaymentMethod() {
        return (FIN_PaymentMethod) get(PROPERTY_PAYMENTMETHOD);
    }

    public void setPaymentMethod(FIN_PaymentMethod paymentMethod) {
        set(PROPERTY_PAYMENTMETHOD, paymentMethod);
    }

    public User getSales() {
        return (User) get(PROPERTY_SALES);
    }

    public void setSales(User sales) {
        set(PROPERTY_SALES, sales);
    }

    public String getNroCheque() {
        return (String) get(PROPERTY_NROCHEQUE);
    }

    public void setNroCheque(String nroCheque) {
        set(PROPERTY_NROCHEQUE, nroCheque);
    }

    public Boolean isEntregado() {
        return (Boolean) get(PROPERTY_ENTREGADO);
    }

    public void setEntregado(Boolean entregado) {
        set(PROPERTY_ENTREGADO, entregado);
    }

    public Boolean isDepositado() {
        return (Boolean) get(PROPERTY_DEPOSITADO);
    }

    public void setDepositado(Boolean depositado) {
        set(PROPERTY_DEPOSITADO, depositado);
    }

    public Date getFechaDeposito() {
        return (Date) get(PROPERTY_FECHADEPOSITO);
    }

    public void setFechaDeposito(Date fechaDeposito) {
        set(PROPERTY_FECHADEPOSITO, fechaDeposito);
    }

    public FIN_FinancialAccount getFINFinancialAccount() {
        return (FIN_FinancialAccount) get(PROPERTY_FINFINANCIALACCOUNT);
    }

    public void setFINFinancialAccount(FIN_FinancialAccount fINFinancialAccount) {
        set(PROPERTY_FINFINANCIALACCOUNT, fINFinancialAccount);
    }

    public FIN_Payment getFINPaymentfrom() {
        return (FIN_Payment) get(PROPERTY_FINPAYMENTFROM);
    }

    public void setFINPaymentfrom(FIN_Payment fINPaymentfrom) {
        set(PROPERTY_FINPAYMENTFROM, fINPaymentfrom);
    }

    public FIN_Payment getFINPaymentto() {
        return (FIN_Payment) get(PROPERTY_FINPAYMENTTO);
    }

    public void setFINPaymentto(FIN_Payment fINPaymentto) {
        set(PROPERTY_FINPAYMENTTO, fINPaymentto);
    }

}
