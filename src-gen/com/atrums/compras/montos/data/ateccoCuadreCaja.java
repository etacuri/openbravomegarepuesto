/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.compras.montos.data;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
/**
 * Entity class for entity atecco_cuadre (stored in table atecco_cuadre).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class ateccoCuadreCaja extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "atecco_cuadre";
    public static final String ENTITY_NAME = "atecco_cuadre";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_ATECCOCIERRECAJA = "ateccoCierrecaja";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_PAYMENTMETHOD = "paymentMethod";
    public static final String PROPERTY_APERTURASISTEMA = "aperturaSistema";
    public static final String PROPERTY_APERTURARESPONSABLE = "aperturaResponsable";
    public static final String PROPERTY_FACTURACION = "facturacion";
    public static final String PROPERTY_CIERRESISTEMA = "cierreSistema";
    public static final String PROPERTY_CIERRERESPONSABLE = "cierreResponsable";
    public static final String PROPERTY_RETIROS = "retiros";
    public static final String PROPERTY_ANTICIPOS = "anticipos";
    public static final String PROPERTY_VALORCIERRE = "valorCierre";
    public static final String PROPERTY_VALIDACION = "validacion";
    public static final String PROPERTY_VALORCORREGIDO = "valorCorregido";
    public static final String PROPERTY_ESTADOCORREGIDO = "estadoCorregido";
    public static final String PROPERTY_BLOQUEADOCORREGIDO = "bloqueadoCorregido";
    public static final String PROPERTY__COMPUTEDCOLUMNS = "_computedColumns";


    // Computed columns properties, these properties cannot be directly accessed, they need
    // to be read through _commputedColumns proxy. They cannot be directly used in HQL, OBQuery
    // nor OBCriteria. 
    public static final String COMPUTED_COLUMN_DIFERENCIA = "diferencia";

    public ateccoCuadreCaja() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_APERTURASISTEMA, new BigDecimal(0));
        setDefaultValue(PROPERTY_APERTURARESPONSABLE, new BigDecimal(0));
        setDefaultValue(PROPERTY_FACTURACION, new BigDecimal(0));
        setDefaultValue(PROPERTY_CIERRESISTEMA, new BigDecimal(0));
        setDefaultValue(PROPERTY_CIERRERESPONSABLE, new BigDecimal(0));
        setDefaultValue(PROPERTY_RETIROS, new BigDecimal(0));
        setDefaultValue(PROPERTY_ANTICIPOS, new BigDecimal(0));
        setDefaultValue(PROPERTY_VALORCIERRE, new BigDecimal(0));
        setDefaultValue(PROPERTY_VALIDACION, "NV");
        setDefaultValue(PROPERTY_ESTADOCORREGIDO, "N");
        setDefaultValue(PROPERTY_BLOQUEADOCORREGIDO, false);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public ateccoCierrecaja getAteccoCierrecaja() {
        return (ateccoCierrecaja) get(PROPERTY_ATECCOCIERRECAJA);
    }

    public void setAteccoCierrecaja(ateccoCierrecaja ateccoCierrecaja) {
        set(PROPERTY_ATECCOCIERRECAJA, ateccoCierrecaja);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public FIN_PaymentMethod getPaymentMethod() {
        return (FIN_PaymentMethod) get(PROPERTY_PAYMENTMETHOD);
    }

    public void setPaymentMethod(FIN_PaymentMethod paymentMethod) {
        set(PROPERTY_PAYMENTMETHOD, paymentMethod);
    }

    public BigDecimal getAperturaSistema() {
        return (BigDecimal) get(PROPERTY_APERTURASISTEMA);
    }

    public void setAperturaSistema(BigDecimal aperturaSistema) {
        set(PROPERTY_APERTURASISTEMA, aperturaSistema);
    }

    public BigDecimal getAperturaResponsable() {
        return (BigDecimal) get(PROPERTY_APERTURARESPONSABLE);
    }

    public void setAperturaResponsable(BigDecimal aperturaResponsable) {
        set(PROPERTY_APERTURARESPONSABLE, aperturaResponsable);
    }

    public BigDecimal getFacturacion() {
        return (BigDecimal) get(PROPERTY_FACTURACION);
    }

    public void setFacturacion(BigDecimal facturacion) {
        set(PROPERTY_FACTURACION, facturacion);
    }

    public BigDecimal getCierreSistema() {
        return (BigDecimal) get(PROPERTY_CIERRESISTEMA);
    }

    public void setCierreSistema(BigDecimal cierreSistema) {
        set(PROPERTY_CIERRESISTEMA, cierreSistema);
    }

    public BigDecimal getCierreResponsable() {
        return (BigDecimal) get(PROPERTY_CIERRERESPONSABLE);
    }

    public void setCierreResponsable(BigDecimal cierreResponsable) {
        set(PROPERTY_CIERRERESPONSABLE, cierreResponsable);
    }

    public BigDecimal getRetiros() {
        return (BigDecimal) get(PROPERTY_RETIROS);
    }

    public void setRetiros(BigDecimal retiros) {
        set(PROPERTY_RETIROS, retiros);
    }

    public BigDecimal getAnticipos() {
        return (BigDecimal) get(PROPERTY_ANTICIPOS);
    }

    public void setAnticipos(BigDecimal anticipos) {
        set(PROPERTY_ANTICIPOS, anticipos);
    }

    public BigDecimal getValorCierre() {
        return (BigDecimal) get(PROPERTY_VALORCIERRE);
    }

    public void setValorCierre(BigDecimal valorCierre) {
        set(PROPERTY_VALORCIERRE, valorCierre);
    }

    public String getValidacion() {
        return (String) get(PROPERTY_VALIDACION);
    }

    public void setValidacion(String validacion) {
        set(PROPERTY_VALIDACION, validacion);
    }

    public BigDecimal getValorCorregido() {
        return (BigDecimal) get(PROPERTY_VALORCORREGIDO);
    }

    public void setValorCorregido(BigDecimal valorCorregido) {
        set(PROPERTY_VALORCORREGIDO, valorCorregido);
    }

    public String getEstadoCorregido() {
        return (String) get(PROPERTY_ESTADOCORREGIDO);
    }

    public void setEstadoCorregido(String estadoCorregido) {
        set(PROPERTY_ESTADOCORREGIDO, estadoCorregido);
    }

    public Boolean isBloqueadoCorregido() {
        return (Boolean) get(PROPERTY_BLOQUEADOCORREGIDO);
    }

    public void setBloqueadoCorregido(Boolean bloqueadoCorregido) {
        set(PROPERTY_BLOQUEADOCORREGIDO, bloqueadoCorregido);
    }

    public String getDiferencia() {
        return (String) get(COMPUTED_COLUMN_DIFERENCIA);
    }

    public void setDiferencia(String diferencia) {
        set(COMPUTED_COLUMN_DIFERENCIA, diferencia);
    }

    public ateccoCuadreCaja_ComputedColumns get_computedColumns() {
        return (ateccoCuadreCaja_ComputedColumns) get(PROPERTY__COMPUTEDCOLUMNS);
    }

    public void set_computedColumns(ateccoCuadreCaja_ComputedColumns _computedColumns) {
        set(PROPERTY__COMPUTEDCOLUMNS, _computedColumns);
    }


    @Override
    public Object get(String propName) {
      if (COMPUTED_COLUMN_DIFERENCIA.equals(propName)) {
        if (get_computedColumns() == null) {
          return null;
        }
        return get_computedColumns().getDiferencia();
      }
    
      return super.get(propName);
    }
}
